import os
import unittest

def add(x,y):
    return x + y

class TestCase(unittest.TestCase):
    def setUp(self):
        app.config.from_object('config')
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        self.app = app.test_client()
        db.create_all()
        pass

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    #tests for each page in site
    def test_index(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_register(self):
        response = self.app.get('/register', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_basket(self):
        response = self.app.get('/basket', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_add_to_store(self):
        response = self.app.get('/add_items', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_change_password(self):
        response = self.app.get('/change_password', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_log_in(self):
        response = self.app.get('/log_in', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
