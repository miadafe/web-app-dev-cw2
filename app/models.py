from app import db, models

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    firstName = db.Column(db.String(500))
    lastName = db.Column(db.String(500))
    password = db.Column(db.String(800))
    email = db.Column(db.String(800))
    itemsInBasket = db.relationship('Item', secondary="basket_items")

class Item(db.Model):
    __tablename__ = 'items'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    itemName = db.Column(db.String(500))
    price = db.Column(db.Float)
    quantity = db.Column(db.Integer)
    buyers = db.relationship('User', secondary="basket_items")

class Basket(db.Model):
    __tablename__ = 'basket_items'
    id = db.Column(db.Integer, primary_key=True, unique=True)
    userId = db.Column(db.Integer, db.ForeignKey('users.id'))
    itemId = db.Column(db.Integer, db.ForeignKey('items.id'))
    user = db.relationship(User, backref=db.backref("basket_items", cascade="all, delete-orphan"))
    item = db.relationship(Item, backref=db.backref("basket_items", cascade="all, delete-orphan"))
