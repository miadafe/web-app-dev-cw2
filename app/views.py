from flask import render_template, flash, request, session, redirect
from app import app, db, models
from .forms import RegistrationForm, LogIn, AddItem, AddToBasket, RemoveFromBasket, ChangePassword
import logging

def signedIn():
    return session.get('loggedIn') == True

def unsignedIn():
    return session.get('loggedIn') is None or session.get('loggedIn') == False


@app.route('/', methods=['GET', 'POST'])
def index():
    app.logger.info('index route request')
    #email used as username
    email = session.get('email')
    items = models.Item.query.all()
    form = AddToBasket()

    if request.method == 'POST':
        itemToAdd = form.id.data
        item = models.Item.query.filter_by(id = itemToAdd).first()

        userid = session.get('id')
        user = models.User.query.filter_by(id = userid).first()

        #relationships
        item.buyers.append(user)
        user.itemsInBasket.append(item)
        db.session.commit()

    return render_template('index.html', title = 'home', email = email,items = items, form=form)

@app.route('/base')
def inheritance():
	return render_template("template_with_inheritance.html")

@app.route('/register', methods=['GET', 'POST'])
def register():
    if signedIn() == True:
        logging.info('User doesnt need to register as theyre already signed in')
        return redirect('/')

    form = RegistrationForm()

    if form.validate_on_submit():
        a = models.User.query.filter_by(email = form.email.data).first()

        if a is None:
            if form.password.data != form.repeatPassword.data:
                app.logger.info('user failed to register')
                flash('Passwords dont match, please try again')
            elif form.password.data == form.repeatPassword.data:
                app.logger.info('new user registered')
                #add user to database
                user = models.User(email = form.email.data, firstName = form.firstName.data, lastName = form.lastName.data, password = form.password.data)
                db.session.add(user)
                db.session.commit()
                flash('Registered successfully')
        else:
            flash('This email is already in our database, try logging in')
    return render_template('register.html', title='Register to the site', form=form)

@app.route('/log_in', methods=['GET', 'POST'])
def log_in():

    if signedIn() == True:
        logging.info('User doesnt need to log in as theyre already logged in')
        return redirect('/')

    form = LogIn()
    if form.validate_on_submit():
        existingUser = models.User.query.filter_by(email=form.email.data).first()

        if existingUser is None:
            flash('%s is not registered in our database. Register first to continue'%(form.email.data))
            app.logger.info('%s failed to log in', form.email.data)
        elif existingUser.password != form.password.data:
            flash('Incorrect password')
            app.logger.info('%s failed to log in', existingUser.email)
        else:
            session['loggedIn'] = True
            session['id'] = existingUser.id
            session['email'] = existingUser.email
            app.logger.info('%s logged in successfully', existingUser.email)
            return redirect('/')

    return render_template('log_in.html', title='Log in', form=form)

@app.route('/logout')
def logout():
    session.pop('loggedIn', None)
    logging.info('user logged out')
    return redirect('/')


@app.route('/basket', methods=['GET', 'POST'])
def basket():

    userid = session.get('id')
    user = models.User.query.filter_by(id=userid).first()

    form = RemoveFromBasket()

    if request.method == 'POST':
        itemToRemove = form.id.data
        itemRemoved = models.Item.query.filter_by(id = itemToRemove).first()

        if user is None or itemRemoved is None:
            logging.warning('nothing to process')
            return redirect('/')
        else:
            #relationship
            userindex = next((i for i, item in enumerate(itemRemoved.buyers) if int(item.id) == int(userid)), -1)
            itemRemoved.buyers.pop(userindex)
            db.session.commit()

    return render_template('basket.html', title = 'Basket', items = user.itemsInBasket, form=form, user= user)


@app.route('/add_items', methods=['GET', 'POST'])
def add_items():
    if unsignedIn() == True:
        logging.warning('Non-admin attempting to add items to site')
        return redirect('/')

    form = AddItem()
    if form.validate_on_submit():
        newItem = models.Item(itemName = form.itemName.data, price = form.price.data, quantity = form.quantity.data)
        db.session.add(newItem)
        db.session.commit()
        flash('Item successfully added to database')
        app.logger.info('admin added new item to database')
    return render_template('add_items.html', title = 'Add Items', form = form)

@app.route('/change_password', methods=['GET', 'POST'])
def change_password():
    form = ChangePassword()

    if form.validate_on_submit():

        userid = session.get('id')
        user = models.User.query.filter_by(id=userid).first()

        if form.newPassword.data != form.repeatNew.data:
            flash("Passwords dont match, please try again")
        elif (user.password != form.oldPassword.data):
            logging.basicConfig(format='%(asctime)s %(message)s')
            logging.info(': failed change password attempt')
            flash("Old password doesn't match password on account")
        elif form.newPassword.data == form.repeatNew.data and user.password == form.oldPassword.data:
            user.password = form.newPassword.data
            db.session.commit()
            app.logger.info('%s changed password', user.email)
            flash("Password changed successfully")

    return render_template('change_password.html', form=form)
