from flask_wtf import Form
from wtforms import IntegerField
from wtforms import DateField
from wtforms import DecimalField
from wtforms import TextField
from wtforms import HiddenField
from wtforms import PasswordField
from wtforms.validators import DataRequired

class RegistrationForm(Form):
	id = HiddenField()
	email = TextField('email', validators = [DataRequired()])
	firstName = TextField('firstName', validators = [DataRequired()])
	lastName = TextField('lastName', validators = [DataRequired()])
	password = PasswordField('password', validators = [DataRequired()])
	repeatPassword = PasswordField('repeatPassword', validators = [DataRequired()])

class LogIn(Form):
	id = TextField('id')
	email = TextField('email', validators = [DataRequired()])
	password = PasswordField('password', validators = [DataRequired()])

class AddItem(Form):
	id = HiddenField()
	itemName = TextField('itemName', validators = [DataRequired()])
	price = DecimalField('price', validators = [DataRequired()])
	quantity = IntegerField('quantity', validators = [DataRequired()])

class AddToBasket(Form):
	id = TextField('id')

class RemoveFromBasket(Form):
	id = TextField('id')

class ChangePassword(Form):
	oldPassword = PasswordField('oldPassword', validators = [DataRequired()])
	newPassword = PasswordField('newPassword', validators = [DataRequired()])
	repeatNew = PasswordField('repeatNew', validators = [DataRequired()])
